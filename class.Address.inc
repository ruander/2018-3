<?php

/**
 * Created by Ruander Oktatóközpont.
 * User: hgy
 * Date: 2018. 06. 15.
 * Time: 14:37
 */
abstract class Address implements Model
{
    //címtípus konstansok
    const ADDRESS_TYPE_RESIDENCE = 1;
    const ADDRESS_TYPE_BUSINESS = 2;
    const ADDRESS_TYPE_TEMPORARY = 3;

    //hibakódok
    const ADDRESS_ERROR_ADDRESS_NOT_FOUND = 1000;

    public static $valid_address_types = [
        self::ADDRESS_TYPE_RESIDENCE => 'Residence',
        self::ADDRESS_TYPE_BUSINESS => 'Business',
        self::ADDRESS_TYPE_TEMPORARY => 'Temporary',
    ];
    //obj tulajdonságok (property)
    //cimsor 1, cimsor 2
    public $street_address_1;
    public $street_address_2;
    //irsz
    protected $_postal_code;
    //város,városrész
    public $city_name;
    public $subdivision_name;
    //ország
    public $country_name;

    //address id,time_created,time_updated
    protected $_address_id;
    protected $_address_type_id;
    protected $_time_created;
    protected $_time_updated;

    /**
     * Address constructor.
     * akkor amikor építjük fel az objektumot (new)
     */
    public function __construct($data = [])
    {
        $this->_time_created = time();
        $this->_init();
        //var_dump($data);

        if (is_array($data) && count($data > 0)) {//objektum felépítése a kapott adattömbből, ha az tömb és legalább 1 eleme van
            foreach ($data as $name => $value) {
                if (in_array($name, ['address_id', 'address_type_id', 'time_created', 'time_updated'])) {//kivételes esetek kezelése a protected elemekre
                    $name = '_' . $name;
                }
                $this->$name = $value;
            }
        } else {
            //különben trigger error
            trigger_error('Nem lehet az objektumot felépíteni a kapott adatokból');
        }
    }

    /**
     * Magic __get
     * akkor fut, amikor egy nem létező vagy védett tulajdonságot próbálunk kiolvasni
     * @param $name
     */
    public function __get($name)
    {
        //var_dump('fut a get, erre:'.$name);
        if (!$this->_postal_code) {
            $this->_postal_code = $this->_postal_code_search();
        }
        //megnézzük nincs e védett ilyen tulajdonság, ha van visszaadjuk azt
        $protected_property_name = '_' . $name;
        if (property_exists($this, $protected_property_name)) {
            return $this->$protected_property_name;
        }
        //ha nincs triggerelünk hibaüzenettel
        trigger_error('Nem létező vagy védett tulajdonságot probálunk elérni __get-tel [' . $name . ']');
    }

    /**
     * Magic __set
     * akkor fut amikor egy nem létező vagy védett tulajdonságot próbálunk beállítani
     * @param $name
     * @param $value
     */
    public function __set($name, $value)
    {
        if ($name == 'postal_code') {
            $this->$name = $value;
            return;
        }

        trigger_error('nem létező vagy védett tulajdonságot próbálunk beállítani: ' . $name . '-[' . $value . ']');

    }

    /**
     * Magic __toString
     * Akkor fut amikor stringgé konvertálnád az objektumot
     * mindenképp string tipussal kell visszatérnie!!!
     * @return string
     */
    public function __toString()
    {
        return $this->display();
    }

    /**
     * Irsz keresése város és városrész alapján
     * @todo implement real database search
     * @return string
     */
    protected function _postal_code_search()
    {
        $db = Database::getInstance();
        $mysqli = $db->getConnection();
        $mysqli->set_charset('utf8');
        //adatok szűrése
        $city_name = $mysqli->real_escape_string($this->city_name);
        $subdivision_name = $mysqli->real_escape_string($this->subdivision_name);
        //lekérés és öszzeállítása
        $qry = "SELECT irsz FROM telepulesek 
                WHERE varos_nev = '$city_name'
                  AND varos_resz = '$subdivision_name' ";

        $result = $mysqli->query($qry) or die($mysqli->error);
        if ($result->num_rows != 1) {//ha nem 1 eredményt kapunk (nem egyértelmű mert
            return 'hiba';
        }
        $row = $result->fetch_object();//stdClass
        //var_dump($row);
        return $row->irsz;
    }

    /**
     * Ajaxos kereső API eljárás
     * @param string to search cities
     * @return array
     */
    public static function addressSearch($string)
    {
        if (is_string($string)) {
            $db = Database::getInstance();
            $mysqli = $db->getConnection();
            $mysqli->set_charset('utf8');
            //adatok szűrése
            $city_name = $mysqli->real_escape_string($string);
            //lekérés és öszzeállítása
            echo $qry = "SELECT * FROM telepulesek 
                WHERE varos_nev LIKE '%$city_name%'";

            $result = $mysqli->query($qry) or die($mysqli->error);
            $rows = $result->fetch_all();//stdClass
            //var_dump($row);
            return json_encode($rows);
        }
        return 'hiba';
    }


    /**
     * Cím kiírása (html)
     * @return string
     */
    public function display()
    {
        $ret = '';
        $ret .= $this->street_address_1;
        //cimsor 2 ha van
        if ($this->street_address_2) {
            $ret .= '<br>' . $this->street_address_2;
        }
        $ret .= '<br>' . $this->postal_code . ', ' . $this->city_name;
        //városrész ha van
        if ($this->subdivision_name) {
            $ret .= ', ' . $this->subdivision_name;
        }
        $ret .= '<br><b>' . $this->country_name . '</b>';
        return $ret;
    }

    //init eljárás kikényszerítése a bővítésekből
    abstract protected function _init();

    /**
     * Egy értékről megmondja érvényes címtipus azonosítónak-e
     * @param $address_type_id
     * @return bool
     */
    public static function isValidAddressTypeId($address_type_id)
    {
        return array_key_exists($address_type_id, self::$valid_address_types);
    }

    /**
     * Címtipus azonosító beállítása
     * @param $address_type_id
     */
    protected function _setAddressTypeId($address_type_id)
    {
        if (self::isValidAddressTypeId($address_type_id)) {
            $this->_address_type_id = $address_type_id;
        }
        return;
    }

    /**
     * Interfaceről megkövetelt eljárások
     */
    public static function load($id)
    {
        $db = Database::getInstance();
        $mysqli = $db->getConnection();
        $mysqli->set_charset('utf8');

        $qry = "SELECT * FROM addresses WHERE address_id = " . (int)$id . " LIMIT 1";
        $result = $mysqli->query($qry) or die($mysqli->error);
        if ($result->num_rows == 0) {
            throw new Exception('Address error not found!', self::ADDRESS_ERROR_ADDRESS_NOT_FOUND);
        }
        $row = $result->fetch_assoc();

        //address_type_id alapján kellene felépíteni az objektumot a megfelelő bővítésen keresztül
        return self::getInstance($row['address_type_id'], $row);
    }

    public static function getInstance($address_type_id = self::ADDRESS_TYPE_RESIDENCE, $data = [])
    {
        //var_dump($address_type_id,$data);
        if (self::isValidAddressTypeId($address_type_id)) {
            //példányosító osztály neve
            $class_name = 'Address' . self::$valid_address_types[$address_type_id];

            //osztály vozsgálata kódból ReflectionClass
            /* $ref = new ReflectionClass($class_name);
             var_dump($ref->isAbstract());
            var_dump($ref->getMethods());
             echo '<pre>'.var_export($ref,true).'</pre>';die();
     */

            $ret = new $class_name($data);
            return $ret;
        }
    }

    /**
     * elem mentése
     * @return inserted element id
     */
    public function save()
    {
        if ($this->_address_id) return $this->update();//ha van id akkor már volt mentve, akkor meg inkább update kell, ne duplikálgassunk :)
        //csatlakozás DBhez
        $db = Database::getInstance();
        $mysqli = $db->getConnection();
        //kódtábla illesztés az ékezetes karakterek miatt
        $mysqli->set_charset("utf8");
        $street_address_1 = $mysqli->real_escape_string($this->street_address_1);
        $street_address_2 = $mysqli->real_escape_string($this->street_address_2);
        $city_name = $mysqli->real_escape_string($this->city_name);
        $subdivision_name = $mysqli->real_escape_string($this->subdivision_name);
        $country_name = $mysqli->real_escape_string($this->country_name);
        echo $qry = "INSERT INTO `addresses` (
                        `address_type_id`, 
                        `street_address_1`, 
                        `street_address_2`, 
                        `postal_code`, 
                        `subdivision_name`, 
                        `city_name`, 
                        `country_name`, 
                        `time_created` 
                        ) VALUES (
                       '$this->_address_type_id', 
                       '$street_address_1', 
                       '$street_address_2', 
                       '$this->postal_code', 
                       '$subdivision_name', 
                       '$city_name', 
                       '$country_name', 
                       '" . date('Y-m-d H:i:s', $this->_time_created) . "')";

        $mysqli->query($qry) or die($mysqli->error);
        $id = $mysqli->insert_id;
        //az objektumba is beírjuk a kapott id-t
        if (is_int($id)) {
            $this->_address_id = $id;
            return $id;
        }
        return false;
    }

    public function update()
    {
        if (!$this->_address_id) return $this->save();//ha nincs id akkor inkább save kell :)
        //csatlakozás DBhez
        $db = Database::getInstance();
        $mysqli = $db->getConnection();
        //kódtábla illesztés az ékezetes karakterek miatt
        $mysqli->set_charset("utf8");
        $street_address_1 = $mysqli->real_escape_string($this->street_address_1);
        $street_address_2 = $mysqli->real_escape_string($this->street_address_2);
        $city_name = $mysqli->real_escape_string($this->city_name);
        $subdivision_name = $mysqli->real_escape_string($this->subdivision_name);
        $country_name = $mysqli->real_escape_string($this->country_name);
        $time_updated = date('Y-m-d H:i:s');
        $qry = "UPDATE addresses 
         SET 
         `address_type_id` = $this->_address_type_id, 
         `street_address_1` = '$street_address_1', 
         `street_address_2` = '$street_address_2', 
         `postal_code` = '$this->postal_code', 
         `subdivision_name` = '$subdivision_name', 
         `city_name` = '$city_name', 
         `country_name` = '$country_name',
         `time_updated` = '$time_updated'
         WHERE `address_id` = $this->_address_id
         ";
        $mysqli->query($qry) or die($mysqli->error);

        return $this->_address_id;
    }

    /**
     * elem törlése
     * @return destroyed element id
     */
    public function delete()
    {
        $db = Database::getInstance();
        $mysqli = $db->getConnection();
        echo $qry = "DELETE FROM addresses WHERE address_id =  $this->_address_id LIMIT 1 ";
        $mysqli->query($qry) or die($mysqli->error);
        return $this->_address_id;
    }

    public static function all()
    {
        $db = Database::getInstance();
        $mysqli = $db->getConnection();
        //kódlap illesztése
        $mysqli->set_charset("utf8");
        //query összeállítása
        //adatok alapján insert query összeállítása
        /*type id helyett a tipus nevét irja ki: */
        $sql_type_id_string = " CASE ";
        foreach (self::$valid_address_types as $type_id => $type_name) {
            $sql_type_id_string .= " WHEN address_type_id =  '$type_id' THEN '$type_name' ";
        }
        $sql_type_id_string .= " END as address_type";
        /********************************/

        $qry = "SELECT address_id,
                      postal_code,
                      city_name,
                      subdivision_name,
                      country_name,
                      street_address_1,
                      street_address_2,
                      $sql_type_id_string
                      FROM addresses";

        $result = $mysqli->query($qry) or die($mysqli->error);

        $rows = $result->fetch_all(MYSQLI_ASSOC);

        return json_encode($rows);
    }

}