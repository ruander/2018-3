<?php
/**
 * Created by Ruander Oktatóközpont.
 * User: hgy
 * Date: 2018. 07. 13.
 * Time: 10:12
 * Címosztály bővítése
 * Állandó lakcím osztály
 */
class AddressResidence extends Address {

    //public $country_name = 'Tiszaszipi';//property redeclare
    //function override
    public function display()
    {
        $ret = '<div class="address-residence col-3">';
        $ret .= parent::display();
        $ret .='</div>';
        return $ret;
    }

    /**
     * címtipus azonosító inicializálása amit a bővítés már meghatároz
     */
    protected function _init(){
        $this->_setAddressTypeId(self::ADDRESS_TYPE_RESIDENCE);
    }
}