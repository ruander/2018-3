<?php
/**
 * Created by Ruander Oktatóközpont.
 * User: hgy
 * Date: 2018. 07. 06.
 * Time: 11:02
 * Adatbázis szingleton
 */
class Database {

    private $_connection;
    private static $_instance;

    public function __construct()
    {
        //kapcsolat felépítése
        $this->_connection = new mysqli('localhost','root','','hgy_oop');

        //hibakezelés
        if(mysqli_connect_error()){
            trigger_error('Hiba az adatbázis kapcsolat felépítése során: '.mysqli_connect_error(),E_USER_ERROR);
        }
    }
    /**
     * Védelem klónozás ellen
     */
    private function __clone()
    {
        // üres clone felülírja a default klónozást
    }

    /**
     * példányosítás (singleton működés)
     * @return db instance
     */
    public static function getInstance(){
        if(!self::$_instance){//ha nincs példány példányosítja magát
            self::$_instance = new self;
        }
        return self::$_instance;
    }

    /**
     * Kapcsolat kiadása
     */
    public function getConnection(){
        return $this->_connection;
    }
}