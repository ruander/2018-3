<?php

interface Model {

    public static function load($id);

    public function save();

    public function update();

    public function delete();

    public static function all();
}