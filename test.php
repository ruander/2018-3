<?php
/**
 * Created by Ruander Oktatóközpont.
 * User: HGy
 * Date: 2018. 06. 08.
 * Time: 10:58
 */
//osztály betöltése
function __autoload($class_name)
{
    //die($class_name);
    include 'class.' . $class_name . '.inc';
}


echo '<h2>Osztálybővítések bemutatása</h2>';
$adatok = [
    'city_name' => 'Tököl',
    'street_address_1' => 'Teszt tér 17.',
    'country_name' => 'Magyarország',
];
$address_residence = new AddressResidence($adatok);
echo $address_residence;
echo '<pre>' . var_export($address_residence, true) . '</pre>';
/*echo '<h2>Ajaxos kereső előkészítése</h2>';
$results = json_decode( Address::addressSearch('ege'));
//echo '<pre>' . var_export($results, true) . '</pre>';
foreach($results as $row){
    echo '<br>'.$row[0].', '.$row[1];
    if($row[2]!='') echo ', '.$row[2];
}*/
echo '<h2>Címtipus azonosítós érvényesség tesztelése</h2>';
for ($i = 0; $i < 5; $i++) {
    echo '<br>' . $i . ': ' . (Address::isValidAddressTypeId($i) ? '' : 'nem') . ' érvényes';
}
echo '<h2>Osztálybővítések bemutatása</h2>';
$adatok = [
    'city_name' => 'Ráckeve',
    'street_address_1' => 'Teszt tér 222.',
    'country_name' => 'Magyarország',
];
$address_business = new AddressBusiness($adatok);
echo $address_business;
$address_business->update();
$address_business->street_address_2 = 'teszteszt222';
$address_business->save();

//echo '<pre>' . var_export($address_business, true) . '</pre>';

$adatok = [
    'city_name' => 'Hódmezővásárhely',
    'street_address_1' => 'Teszt tér 17.',
    'country_name' => 'Magyarország',
];
$address_temporary = new AddressTemporary($adatok);
echo $id = $address_temporary->save();

echo '<pre>' . var_export($address_temporary, true) . '</pre>';

echo '<h2>statikus load() | (db-ből betöltés) tesztelése</h2>';
try {
    $address_db = Address::load($id);
    echo "ID:$id" . $address_db;
} catch (Exception $e) {
    echo $e->getMessage() . '[' . $e->getCode() . ']';
}
////CRUD
$addresses = Address::all();
$labels = '';
$table = '<table class="table table-responsive table-striped">';
if ($addresses = json_decode($addresses,true)) {

    foreach ($addresses as $address) {
        if (empty($labels)) {
            $labels = '<tr><th>' . implode('</th><th>', str_replace('_',' ',array_keys($address))) . '</th></tr>';
            $table .= $labels;
        }

        $table .= '<tr><td>' . implode('</td><td>', $address) . '</td></tr>';
    }
    $table .= '</table>';
    echo $table;

}



